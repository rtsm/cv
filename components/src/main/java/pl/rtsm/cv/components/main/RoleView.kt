package pl.rtsm.cv.components.main

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.view_role.view.*
import org.jetbrains.anko.dimen
import pl.rtsm.cv.components.R
import pl.rtsm.cv.core.model.ExperienceData

class RoleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_role, this)
        setPadding(paddingLeft, dimen(R.dimen.role_view_padding), paddingRight, paddingBottom)
    }

    fun setModel(model: ExperienceData) {
        role.text = model.role
        if(!model.organization.isNullOrEmpty()) {
            if(!model.location.isNullOrEmpty()) {
                company.text = "${model.organization} (${model.location})"
            } else {
                company.text = model.organization
            }
            company.visibility = View.VISIBLE
        } else {
            company.visibility = View.GONE
        }
        date.text = "${model.started} - ${model.finished ?: context.getString(R.string.present)}"
        technologiesRecyclerView.setElements(model.technologies)
    }

}