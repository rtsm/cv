package pl.rtsm.cv.components.main

import android.content.Context
import android.util.AttributeSet
import android.view.View
import kotlinx.android.synthetic.main.view_education_data.view.*
import pl.rtsm.cv.components.R
import pl.rtsm.cv.core.model.EducationData

class EducationDataView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): MainCardView(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_education_data, this)
    }

    fun setModel(model: EducationData) {
        organization.text = model.organization
        unit.text = model.unit
        major.text = model.major
        date.text = "${model.started} - ${model.finished}"
    }
}