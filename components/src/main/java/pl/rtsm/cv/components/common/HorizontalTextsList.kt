package pl.rtsm.cv.components.common

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.setPadding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.dimen
import pl.rtsm.cv.components.R
import pl.rtsm.cv.core.extensions.color
import pl.rtsm.cv.core.extensions.setMargin

class HorizontalTextsList @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    init {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    fun setElements(elements: List<String>) {
        adapter = StringAdapter(elements)
    }

    private class StringAdapter(private val elements: List<String>): Adapter<StringViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder {
            return StringViewHolder(TextView(parent.context).apply {
                setTextColor(color(android.R.color.white))
                setPadding(dimen(R.dimen.string_list_card_padding))
                val margin = dimen(R.dimen.string_list_card_margin)
                setMargin(null, margin, margin, margin)
                setBackgroundResource(R.drawable.string_list_element_background)
            })
        }

        override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
            holder.textView.text = elements[position]
        }
        override fun getItemCount(): Int = elements.size
    }

    private class StringViewHolder(val textView: TextView): ViewHolder(textView)
}