package pl.rtsm.cv.components.main

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.cardview.widget.CardView
import org.jetbrains.anko.dimen
import pl.rtsm.cv.components.R
import pl.rtsm.cv.core.extensions.setMargin

abstract class MainCardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    init {
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        radius = dimen(R.dimen.main_card_corner_radius).toFloat()
        setMargin(dimen(R.dimen.main_card_padding))
    }
}