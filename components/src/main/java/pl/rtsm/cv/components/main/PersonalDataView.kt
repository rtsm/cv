package pl.rtsm.cv.components.main

import android.content.Context
import android.util.AttributeSet
import android.view.View
import kotlinx.android.synthetic.main.view_personal_data.view.*
import pl.rtsm.cv.components.R
import pl.rtsm.cv.core.model.PersonalData

class PersonalDataView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): MainCardView(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_personal_data, this)
    }

    fun setModel(model: PersonalData) {
        phoneNo.text = model.phone
        email.text = model.email
        interestsRecyclerView.setElements(model.interests)
        skillsRecyclerView.setElements(model.skills)
    }
}