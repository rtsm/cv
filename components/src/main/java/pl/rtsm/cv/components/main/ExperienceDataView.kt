package pl.rtsm.cv.components.main

import android.content.Context
import android.util.AttributeSet
import android.view.View
import kotlinx.android.synthetic.main.view_experience_data.view.*
import pl.rtsm.cv.components.R
import pl.rtsm.cv.core.model.ExperienceData

class ExperienceDataView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): MainCardView(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_experience_data, this)
    }

    fun setModel(model: List<ExperienceData>) {
        model.reversed().forEach {
            rolesContainer.addView(RoleView(context).apply {
                setModel(it)
            })
        }
    }
}