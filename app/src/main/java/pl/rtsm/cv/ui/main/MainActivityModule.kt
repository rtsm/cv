package pl.rtsm.cv.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.rtsm.cv.di.FragmentScope

@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [MainFragmentModule::class])
    abstract fun bindMainFragment(): MainFragment
}