package pl.rtsm.cv.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_scrolling.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.profile_header.*
import org.jetbrains.anko.alert
import pl.rtsm.cv.R
import pl.rtsm.cv.base.BaseFragment
import pl.rtsm.cv.components.main.EducationDataView
import pl.rtsm.cv.components.main.ExperienceDataView
import pl.rtsm.cv.components.main.PersonalDataView
import pl.rtsm.cv.core.model.ProfileResource
import javax.inject.Inject

class MainFragment : BaseFragment(), MainContract.View {

    @Inject
    lateinit var presenter: MainContract.Presenter

    @Inject
    lateinit var picasso: Picasso

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onResume() {
        super.onResume()
        presenter.attach(this)
        presenter.load()
    }

    override fun onPause() {
        presenter.detach()
        super.onPause()
    }

    override fun showProfile(profile: ProfileResource) {
        picasso.load(profile.personal.photo).into(profileImage)
        profileName.text = profile.personal.name
        scrollViewContainer.apply {
            removeAllViews()
            addView(PersonalDataView(context!!).apply {
                setModel(profile.personal)
            })
            addView(EducationDataView(context!!).apply {
                setModel(profile.education.first())
            })
            addView(ExperienceDataView(context!!).apply {
                setModel(profile.experience)
            })
        }
    }

    override fun showLoader(show: Boolean) {
        progressBar.isVisible = show
        scrollViewContainer.isVisible != show
    }

    override fun showError() {
        context?.alert(R.string.dialog_message_error_occurred, R.string.dialog_title_error) {
            positiveButton(R.string.button_try_again) {
                it.dismiss()
                presenter.load()
            }
            negativeButton(R.string.button_exit_app) {
                activity?.finish()
            }
            isCancelable = false
            show()
        }
    }
}