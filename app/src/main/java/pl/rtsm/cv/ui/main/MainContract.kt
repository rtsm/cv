package pl.rtsm.cv.ui.main

import pl.rtsm.cv.base.BasePresenter
import pl.rtsm.cv.core.model.ProfileResource

interface MainContract {

    interface View {
        fun showLoader(show: Boolean)
        fun showProfile(profile: ProfileResource)
        fun showError()

    }

    interface Presenter : BasePresenter<View> {
        fun load()
    }
}