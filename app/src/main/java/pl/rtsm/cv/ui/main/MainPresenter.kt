package pl.rtsm.cv.ui.main

import io.reactivex.disposables.Disposable
import pl.rtsm.cv.core.extensions.ioTransform
import pl.rtsm.cv.usecase.GetProfileUseCase
import timber.log.Timber
import javax.inject.Inject

class MainPresenter @Inject constructor(
        private val getProfileUseCase: GetProfileUseCase
) : MainContract.Presenter {

    private var disposable: Disposable? = null
    private var view: MainContract.View? = null

    override fun attach(view: MainContract.View) {
        this.view = view
    }

    override fun detach() {
        disposable?.dispose()
        view = null
    }

    override fun load() {
        disposable?.dispose()
        disposable = getProfileUseCase
                .get()
                .ioTransform()
                .doOnSubscribe { view?.showLoader(true) }
                .doOnTerminate { view?.showLoader(false) }
                .subscribe(
                        {
                            view?.showProfile(it)
                        },
                        {
                            Timber.d("Failed to fetch profile: $it")
                            view?.showError()
                        }
                )
    }
}
