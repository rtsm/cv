package pl.rtsm.cv.ui.main

import android.os.Bundle

import pl.rtsm.cv.R
import pl.rtsm.cv.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}