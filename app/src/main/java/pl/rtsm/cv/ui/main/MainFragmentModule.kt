package pl.rtsm.cv.ui.main

import dagger.Binds
import dagger.Module
import pl.rtsm.cv.di.FragmentScope

@Module
abstract class MainFragmentModule {

    @FragmentScope
    @Binds
    abstract fun mainPresenter(presenter: MainPresenter): MainContract.Presenter
}
