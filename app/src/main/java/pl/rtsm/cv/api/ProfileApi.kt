package pl.rtsm.cv.api

import io.reactivex.Single
import pl.rtsm.cv.core.model.ProfileResource
import retrofit2.http.GET

interface ProfileApi {
    @GET("rtsm/cv/raw/master/cv.json")
    fun getProfile(): Single<ProfileResource>
}