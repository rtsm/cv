package pl.rtsm.cv.usecase

import io.reactivex.Observable
import pl.rtsm.cv.base.BaseUseCase
import pl.rtsm.cv.core.model.ProfileResource
import pl.rtsm.cv.data.repositories.ProfileRepository
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
        private val repository: ProfileRepository
) : BaseUseCase<Unit, ProfileResource> {
    override fun get(): Observable<ProfileResource> {
        return repository.getProfile().toObservable()
    }

}