package pl.rtsm.cv.di

import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilsModule {

    @Provides
    @Singleton
    fun providePicasso(): Picasso = Picasso.get()
}