package pl.rtsm.cv.di

import android.content.Context
import dagger.Binds
import dagger.Module
import pl.rtsm.cv.App

@Module
interface AppModule {
    @Binds
    fun bindContext(application: App): Context
}