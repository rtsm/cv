package pl.rtsm.cv.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.rtsm.cv.ui.main.MainActivity
import pl.rtsm.cv.ui.main.MainActivityModule


@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

}