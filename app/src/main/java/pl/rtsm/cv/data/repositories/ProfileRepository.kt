package pl.rtsm.cv.data.repositories

import io.reactivex.Single
import pl.rtsm.cv.api.ProfileApi
import pl.rtsm.cv.core.model.ProfileResource
import javax.inject.Inject

class ProfileRepository @Inject constructor(
        private val api: ProfileApi
) {
    fun getProfile(): Single<ProfileResource> {
        return api.getProfile()
    }
}