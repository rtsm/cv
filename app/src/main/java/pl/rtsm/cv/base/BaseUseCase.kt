package pl.rtsm.cv.base

import io.reactivex.Observable

interface BaseUseCase<R, T> {
    fun setArg(arg: R) = Unit
    fun get(): Observable<T>
}