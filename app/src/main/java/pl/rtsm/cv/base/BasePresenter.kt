package pl.rtsm.cv.base

interface BasePresenter<V> {
    fun attach(view: V)
    fun detach()
}