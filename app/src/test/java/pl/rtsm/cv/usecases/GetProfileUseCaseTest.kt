package pl.rtsm.cv.usecases

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import pl.rtsm.cv.TestSchedulersRule
import pl.rtsm.cv.createTestProfile
import pl.rtsm.cv.data.repositories.ProfileRepository
import pl.rtsm.cv.usecase.GetProfileUseCase
import java.io.IOException
import java.util.concurrent.TimeUnit

@ExtendWith(TestSchedulersRule::class)
class GetProfileUseCaseTest {

    lateinit var repo: ProfileRepository
    lateinit var useCase: GetProfileUseCase

    @BeforeEach
    fun setUp() {
        repo = mock()
        useCase = GetProfileUseCase(repo)
    }

    @Test
    fun get_profile_success() {
        val testScheduler = TestScheduler()
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }

        whenever(repo.getProfile()).thenReturn(Single.just(createTestProfile()))
        val testSubscriber = useCase.get()
                .subscribeOn(testScheduler)
                .observeOn(testScheduler)
                .test()
        testSubscriber.assertNoValues()
        useCase.get()
        testScheduler.triggerActions()
        testSubscriber.assertValue(createTestProfile())

        verify(repo, times(2)).getProfile()
        verifyNoMoreInteractions(repo)
    }

    @Test
    fun get_error_when_loading_returns_error() {
        val testScheduler = TestScheduler()
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }

        val ioException = IOException()
        whenever(repo.getProfile()).thenReturn(Single.error(ioException))

        useCase.get()
        val testSubscriber = useCase.get()
                .subscribeOn(testScheduler)
                .observeOn(testScheduler)
                .test()

        testScheduler.triggerActions()
        testScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)
        testSubscriber.assertError(ioException)
    }
}