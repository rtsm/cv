package pl.rtsm.cv.repositories

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import pl.rtsm.cv.TestSchedulersRule
import pl.rtsm.cv.api.ProfileApi
import pl.rtsm.cv.createTestProfile
import pl.rtsm.cv.data.repositories.ProfileRepository
import java.io.IOException

@ExtendWith(TestSchedulersRule::class)
class ProfileRepositoryTest {

    lateinit var api: ProfileApi
    lateinit var repository: ProfileRepository

    @BeforeEach
    fun setUp() {
        api = mock()
        repository = ProfileRepository(api)
    }

    @Test
    fun getProfiles_success_data() {
        whenever(api.getProfile()).thenReturn(
                Single.just(createTestProfile())
        )

        repository.getProfile()
                .test()
                .awaitCount(1)
                .assertValue(createTestProfile())

        verify(api).getProfile()
        verifyNoMoreInteractions(api)
    }

    @Test
    fun getProfiles_error_data() {
        val ioException = IOException()
        whenever(api.getProfile()).thenReturn(
                Single.error(ioException)
        )

        repository.getProfile()
                .test()
                .awaitCount(1)
                .assertError(ioException)
    }

}