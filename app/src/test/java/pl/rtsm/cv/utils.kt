package pl.rtsm.cv

import pl.rtsm.cv.core.model.EducationData
import pl.rtsm.cv.core.model.ExperienceData
import pl.rtsm.cv.core.model.PersonalData
import pl.rtsm.cv.core.model.ProfileResource

fun createTestProfile(): ProfileResource {
    val personal = PersonalData(
            "Test Test",
            "+0142424242",
            "test@example.com",
            "https://example.com/photo.jpg",
            listOf("Interest1", "Interest2"),
            listOf("skill1", "skill2"))
    val educationData = EducationData(
            "Test University",
            "Test Unit",
            "Testing Science",
            "01.01.2000",
            "01.01.2010")
    val experience = ExperienceData(
            "Test Corp",
            "Test City",
            "COO", emptyList(),
            "01.01.2011",
            "02.01.2012")
    return ProfileResource(personal, listOf(educationData), listOf(experience))
}