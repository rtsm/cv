package pl.rtsm.cv.ui.main

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import pl.rtsm.cv.TestSchedulersRule
import pl.rtsm.cv.createTestProfile
import pl.rtsm.cv.usecase.GetProfileUseCase
import java.io.IOException

@ExtendWith(TestSchedulersRule::class)
class MainPresenterTest {

    val view = mock<MainContract.View>()
    val useCase: GetProfileUseCase = mock()
    val presenter = MainPresenter(useCase)

    @BeforeEach
    fun setUp() {
        whenever(useCase.get()).thenReturn(Observable.just(createTestProfile()))
    }

    @Test
    fun load_success_shows_profile() {
        presenter.attach(view)
        presenter.load()

        verify(view).showLoader(true)
        verify(view).showProfile(createTestProfile())
        verify(view).showLoader(false)
        verifyNoMoreInteractions(view)
    }

    @Test
    fun load_error_shows_error() {
        whenever(useCase.get()).thenReturn(Observable.error(IOException()))
        presenter.attach(view)
        presenter.load()

        verify(view).showLoader(true)
        verify(view).showError()
        verify(view).showLoader(false)
        verifyNoMoreInteractions(view)
    }
}