package pl.rtsm.cv.core.model

import com.google.gson.annotations.SerializedName

data class ExperienceData(
        @SerializedName("organization") val organization: String? = null,
        @SerializedName("location") val location: String? = null,
        @SerializedName("role") val role: String,
        @SerializedName("technologies") val technologies: List<String>,
        @SerializedName("started") val started: String,
        @SerializedName("finished") val finished: String? = null
)