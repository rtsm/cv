package pl.rtsm.cv.core.extensions

import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.view.marginBottom
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.core.view.marginTop

fun View.setMargin(margin: Int) {
    val params = if (layoutParams != null)
        ViewGroup.MarginLayoutParams(layoutParams)
    else
        ViewGroup.MarginLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    layoutParams = params.apply {
        setMargins(margin, margin, margin, margin)
    }
}

fun View.setMargin(left: Int? = null, top: Int? = null, right: Int? = null, bottom: Int? = null) {
    val params = if (layoutParams != null)
        ViewGroup.MarginLayoutParams(layoutParams)
    else
        ViewGroup.MarginLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    layoutParams = params.apply {
        setMargins(left ?: marginLeft, top ?: marginTop, right?: marginRight, bottom?: marginBottom)
    }
}

fun View.color(@ColorRes id: Int): Int {
    return ContextCompat.getColor(context, id)
}