package pl.rtsm.cv.core.model

import com.google.gson.annotations.SerializedName

data class EducationData(
        @SerializedName("organization") val organization: String,
        @SerializedName("unit") val unit: String,
        @SerializedName("major") val major: String,
        @SerializedName("started") val started: String,
        @SerializedName("finished") val finished: String
)