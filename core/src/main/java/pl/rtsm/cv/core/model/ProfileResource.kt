package pl.rtsm.cv.core.model

import com.google.gson.annotations.SerializedName

data class ProfileResource (
        @SerializedName("personal") val personal : PersonalData,
        @SerializedName("education") val education : List<EducationData>,
        @SerializedName("experience") val experience : List<ExperienceData>
)