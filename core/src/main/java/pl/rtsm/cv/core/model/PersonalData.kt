package pl.rtsm.cv.core.model

import com.google.gson.annotations.SerializedName

data class PersonalData (

        @SerializedName("name") val name : String,
        @SerializedName("phone") val phone : String,
        @SerializedName("email") val email : String,
        @SerializedName("photo") val photo : String,
        @SerializedName("interests") val interests : List<String>,
        @SerializedName("skills") val skills : List<String>
)